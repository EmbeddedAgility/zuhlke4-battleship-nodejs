class Board {
  constructor(width, length) {
    this.fields = [];
    for (let i = 0; i <= width; i++) {
      this.fields.push(new Array(length).fill(""));
    }
  }
}

module.exports = Board;
