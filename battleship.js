const { Worker, isMainThread } = require("worker_threads");
const readline = require("readline-sync");
const gameController = require("./GameController/gameController.js");
const cliColor = require("cli-color");
const beep = require("beepbeep");
const position = require("./GameController/position.js");
const letters = require("./GameController/letters.js");
const { off } = require("process");
const Board = require("./Board/Board.js");
const checkInput = require("./helpers.js");
let telemetryWorker;
let totalShipSize = 0;
let cheatExecuted = false;

class Battleship {
  constructor() {
    this.board = new Board(8, 8);
  }

  start() {
    telemetryWorker = new Worker("./TelemetryClient/telemetryClient.js");

    console.log("Starting...");
    telemetryWorker.postMessage({
      eventName: "ApplicationStarted",
      properties: { Technology: "Node.js" },
    });

    console.log(cliColor.magenta("                                     |__"));
    console.log(cliColor.magenta("                                     |\\/"));
    console.log(cliColor.magenta("                                     ---"));
    console.log(cliColor.magenta("                                     / | ["));
    console.log(cliColor.magenta("                              !      | |||"));
    console.log(
      cliColor.magenta("                            _/|     _/|-++'")
    );
    console.log(
      cliColor.magenta("                        +  +--|    |--|--|_ |-")
    );
    console.log(
      cliColor.magenta("                     { /|__|  |/\\__|  |--- |||__/")
    );
    console.log(
      cliColor.magenta(
        "                    +---------------___[}-_===_.'____                 /\\"
      )
    );
    console.log(
      cliColor.magenta(
        "                ____`-' ||___-{]_| _[}-  |     |_[___\\==--            \\/   _"
      )
    );
    console.log(
      cliColor.magenta(
        " __..._____--==/___]_|__|_____________________________[___\\==--____,------' .7"
      )
    );
    console.log(
      cliColor.magenta(
        "|                        Welcome to Battleship                         BB-61/"
      )
    );
    console.log(
      cliColor.magenta(
        " \\_________________________________________________________________________|"
      )
    );
    console.log();

    this.InitializeGame();
    this.StartGame();
  }

  StartGame() {
    console.clear();
    console.log("                  __");
    console.log("                 /  \\");
    console.log("           .-.  |    |");
    console.log("   *    _.-'  \\  \\__/");
    console.log("    \\.-'       \\");
    console.log("   /          _/");
    console.log("  |      _  /");
    console.log("  |     /_\\'");
    console.log("   \\    \\_/");
    console.log('    """"');

    let enemyFleetHit = [];
    let myFleetHit = [];

    do {
      if (cheatExecuted) {
        console.clear();
        console.log(cliColor.blue("Cheat applied"));
        cheatExecuted = false;
      }
      console.log();
      console.log("Player, it's your turn");
      console.log("Enter coordinates for your shot :");
      var position = Battleship.ParsePosition(readline.question());
      var isHit = gameController.CheckIsHit(this.enemyFleet, position);

      telemetryWorker.postMessage({
        eventName: "Player_ShootPosition",
        properties: { Position: position.toString(), IsHit: isHit },
      });

      if (isHit) {
        enemyFleetHit.push(position.toString());
        beep();

        console.log("                \\         .  ./");
        console.log('              \\      .:";\'.:.."   /');
        console.log("                  (M^^.^~~:.'\").");
        console.log("            -   (/  .    . . \\ \\)  -");
        console.log("               ((| :. ~ ^  :. .|))");
        console.log("            -   (\\- |  \\ /  |  /)  -");
        console.log("                 -\\  \\     /  /-");
        console.log("                   \\  \\   /  /");
      }

      console.log(isHit ? "Yeah ! Nice hit !" : "Miss");

      var computerPos = this.GetRandomPosition();
      var isHit = gameController.CheckIsHit(this.myFleet, computerPos);

      telemetryWorker.postMessage({
        eventName: "Computer_ShootPosition",
        properties: { Position: computerPos.toString(), IsHit: isHit },
      });

      console.log();
      console.log(
        `Computer shot in ${computerPos.column}${computerPos.row} and ` +
          (isHit ? `has hit your ship !` : `miss`)
      );
      if (isHit) {
        beep();
        myFleetHit.push(position.toString());

        console.log("                \\         .  ./");
        console.log('              \\      .:";\'.:.."   /');
        console.log("                  (M^^.^~~:.'\").");
        console.log("            -   (/  .    . . \\ \\)  -");
        console.log("               ((| :. ~ ^  :. .|))");
        console.log("            -   (\\- |  \\ /  |  /)  -");
        console.log("                 -\\  \\     /  /-");
        console.log("                   \\  \\   /  /");
      }

      if (this.isGameOver(enemyFleetHit, myFleetHit)) {
        console.log("Game over");
        if (enemyFleetHit.length == totalShipSize) {
          this.gameWonGraph();
        }
        if (myFleetHit.length == totalShipSize) {
          console.log("You lost!");
        }
        beep();

        telemetryWorker.terminate();
        break;
      }
    } while (true);
  }

  isGameOver(enemyFleetHits, myFleetHits) {
    let enemyFleetHitSet = new Set(enemyFleetHits);
    let myFleetHitSet = new Set(myFleetHits);
    if (enemyFleetHitSet.size === 17 || myFleetHitSet.size === 17) return true;
    return false;
  }

  gameWonGraph() {
    console.log(
      "                   " + cliColor.green("YOU ARE THE WINNER!!!")
    );
    console.log("                  _         ___");
    console.log("                 / \\       /   \\");
    console.log("                  \\ \\     |     |");
    console.log("                    \\ \\   \\    /");
    console.log("                      \\ \\__|  |______");
    console.log("                       \\          |\\ \\");
    console.log("                        |         | \\ \\");
    console.log("                        |         |  \\ \\");
    console.log("                        |         |   \\ \\");
    console.log("                        |         |");
    console.log("                        |         |");
    console.log("                        |         |");
    console.log("                        /  ____   |");
    console.log("                       /  /    \\ \\");
    console.log("                      /  /      \\ \\");
    console.log("                     /  /        \\ \\");
    console.log("                    /  /          \\ \\");
    console.log("                   /  /            \\ \\");
    console.log("                  /  /              \\ \\");
    console.log("         ___     /__/                \\_\\");
    console.log("        /, ,\\");
    console.log("        \\___/   / /                    ___");
    console.log("        /   \\__/ /____________________/ _/");
    console.log(`       |  ${cliColor.red("Kme Kme, I lost")} ____________/ /`);
    console.log("        \\______________________________/");
  }

  static ParsePosition(input) {
    var letter = letters.get(input.toUpperCase().substring(0, 1));
    var number = parseInt(input.substring(1, 2), 10);
    return new position(letter, number);
  }

  GetRandomPosition() {
    var rows = 8;
    var lines = 8;
    var rndColumn = Math.floor(Math.random() * lines);
    var letter = letters.get(rndColumn + 1);
    var number = Math.floor(Math.random() * rows);
    var result = new position(letter, number);
    return result;
  }

  InitializeGame() {
    this.InitializeMyFleet();
    this.InitializeEnemyFleet();
  }

  InitializeMyFleet() {
    this.myFleet = gameController.InitializeShips();

    console.log(
      "Please position your fleet (Game board size is from A to H and 1 to 8) :"
    );
    cheatExecuted = false;

    const checkInputBoard = (ship) =>
      checkInput(ship, this.board, telemetryWorker);

    !cheatExecuted &&
      this.myFleet.forEach(function (ship) {
        console.log();
        console.log(
          `Please enter the positions for the ${ship.name} (size: ${ship.size})`
        );
        checkInputBoard(ship);
      });

    cheatExecuted &&
      this.myFleet.forEach((ship) => {
        totalShipSize += ship.size;
        if (cheatExecuted) return;
        console.log();
        console.log(
          `Please enter the positions for the ${ship.name} (size: ${ship.size})`
        );
        for (var i = 1; i < ship.size + 1; i++) {
          console.log(`Enter position ${i} of ${ship.size} (i.e A3):`);
          const position = readline.question();
          if (this.isInputACheat(position)) {
            cheatExecuted = this.execCheat(position);
            if (cheatExecuted) break;
          }
          telemetryWorker.postMessage({
            eventName: "Player_PlaceShipPosition",
            properties: {
              Position: position,
              Ship: ship.name,
              PositionInShip: i,
            },
          });
          ship.addPosition(Battleship.ParsePosition(position));
        }
      });
  }

  InitializeEnemyFleet() {
    this.enemyFleet = gameController.InitializeShips();

    this.enemyFleet[0].addPosition(new position(letters.B, 4));
    this.enemyFleet[0].addPosition(new position(letters.B, 5));
    this.enemyFleet[0].addPosition(new position(letters.B, 6));
    this.enemyFleet[0].addPosition(new position(letters.B, 7));
    this.enemyFleet[0].addPosition(new position(letters.B, 8));

    this.enemyFleet[1].addPosition(new position(letters.E, 6));
    this.enemyFleet[1].addPosition(new position(letters.E, 7));
    this.enemyFleet[1].addPosition(new position(letters.E, 8));
    this.enemyFleet[1].addPosition(new position(letters.E, 9));

    this.enemyFleet[2].addPosition(new position(letters.A, 3));
    this.enemyFleet[2].addPosition(new position(letters.B, 3));
    this.enemyFleet[2].addPosition(new position(letters.C, 3));

    this.enemyFleet[3].addPosition(new position(letters.F, 8));
    this.enemyFleet[3].addPosition(new position(letters.G, 8));
    this.enemyFleet[3].addPosition(new position(letters.H, 8));

    this.enemyFleet[4].addPosition(new position(letters.C, 5));
    this.enemyFleet[4].addPosition(new position(letters.C, 6));
  }

  isInputACheat(userInput) {
    return userInput.charAt(0) === "!";
  }

  execCheat(userInput) {
    const cheat = userInput.slice(1).toLowerCase();
    switch (cheat) {
      case "iamtoolazy":
        this.insertUserShips();
        return true;
      default:
        return false;
    }
  }

  insertUserShips() {
    this.myFleet[0].addPosition(new position(letters.A, 4));
    this.myFleet[0].addPosition(new position(letters.A, 5));
    this.myFleet[0].addPosition(new position(letters.A, 6));
    this.myFleet[0].addPosition(new position(letters.A, 7));
    this.myFleet[0].addPosition(new position(letters.A, 8));

    this.myFleet[1].addPosition(new position(letters.B, 6));
    this.myFleet[1].addPosition(new position(letters.B, 7));
    this.myFleet[1].addPosition(new position(letters.B, 8));
    this.myFleet[1].addPosition(new position(letters.B, 9));

    this.myFleet[2].addPosition(new position(letters.c, 2));
    this.myFleet[2].addPosition(new position(letters.c, 3));
    this.myFleet[2].addPosition(new position(letters.C, 4));

    this.myFleet[3].addPosition(new position(letters.F, 8));
    this.myFleet[3].addPosition(new position(letters.G, 8));
    this.myFleet[3].addPosition(new position(letters.H, 8));

    this.myFleet[4].addPosition(new position(letters.G, 7));
    this.myFleet[4].addPosition(new position(letters.G, 8));
  }
}

module.exports = Battleship;
