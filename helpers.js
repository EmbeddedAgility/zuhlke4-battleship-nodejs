const letters = require("./GameController/letters.js");
const readline = require("readline-sync");

const calcPlace = (position, num) => {
  let keys = Object.keys(letters).filter((x) => letters[x].value === position);

  if (keys.length > 0) {
    return `${keys[0]}${num}`;
  }
};

const checkInput = (ship, board, telemetryWorker) => {
  const position = readline.question();
  const splitted = position.split("");

  if (splitted.length < 2) {
    checkInput(ship);
    return;
  }

  const letterToNum = letters[splitted[0]].value;
  let canFitX = true;
  let canFitMinusX = true;
  let canFitY = true;
  let canFitMinusY = true;

  if (board.fields[letterToNum - 1][parseInt(splitted[1]) - 1] !== "") {
    console.log(
      "There is already ship at this place, please enter different spot"
    );
    checkInput(ship, board, telemetryWorker);
    return;
  }

  for (let i = 1; i <= ship.size; i++) {
    if (
      board.fields[letterToNum - 1][parseInt(splitted[1]) - 1 - i] !== "" ||
      board.fields[letterToNum - 1][parseInt(splitted[1]) - 1 - i] === undefined
    ) {
      canFitMinusX = false;
    }
    if (
      board.fields[letterToNum - 1][parseInt(splitted[1]) - 1 + i] !== "" ||
      board.fields[letterToNum - 1][parseInt(splitted[1]) - 1 + i] === undefined
    ) {
      canFitX = false;
    }
    if (
      board.fields[letterToNum - 1 - i] === undefined ||
      board.fields[letterToNum - 1 - i][parseInt(splitted[1]) - 1] !== "" ||
      board.fields[letterToNum - 1 - i][parseInt(splitted[1]) - 1] === undefined
    ) {
      canFitMinusY = false;
    }
    if (
      board.fields[letterToNum - 1 + i] === undefined ||
      board.fields[letterToNum - 1 + i][parseInt(splitted[1]) - 1] !== "" ||
      board.fields[letterToNum - 1 + i][parseInt(splitted[1]) - 1] === undefined
    ) {
      canFitY = false;
    }
  }

  if (canFitX || canFitMinusX || canFitY || canFitMinusY) {
    let question = "Your ship can fit on next locations: ";
    let chosen = [];

    if (canFitX) {
      const isInField = parseInt(splitted[1]) + ship.size;

      if (isInField > 0) {
        const calculatedPosition = calcPlace(letterToNum, isInField);
        question += `${position} - ${calculatedPosition} `;
        chosen.push(calculatedPosition);
      }
    }

    if (canFitMinusX) {
      const isInField = parseInt(splitted[1]) - ship.size;
      if (isInField > 0) {
        const calculatedPosition = calcPlace(letterToNum, isInField);
        question += `${position} - ${calculatedPosition} `;
        chosen.push(calculatedPosition);
      }
    }
    if (canFitY) {
      const isInField = letterToNum + ship.size;

      if (isInField <= 8) {
        const calculatedPosition = calcPlace(
          letterToNum + ship.size,
          parseInt(splitted[1])
        );
        question += `${position} - ${calculatedPosition} `;
        chosen.push(calculatedPosition);
      }
    }
    if (canFitMinusY) {
      const isInField = letterToNum - ship.size;
      if (isInField <= 8 && isInField > 0) {
        const calculatedPosition = calcPlace(
          letterToNum - ship.size,
          parseInt(splitted[1])
        );
        question += `${position} - ${calculatedPosition}`;
        chosen.push(calculatedPosition);
      }
    }

    console.log(
      question + " (choose only ending field eg. (A1- F1) just enter F1)"
    );

    const chosenPosition = readline.question();

    const selected = chosen.filter((field) => field === chosenPosition);

    board.fields[letterToNum - 1][parseInt(splitted[1]) - 1] = {
      ship,
      hasBeenHit: false,
    };
    telemetryWorker.postMessage({
      eventName: "Player_PlaceShipPosition",
      properties: {
        Position: `${letterToNum - 1}${splitted[1] - 1}`,
        Ship: ship.name,
        PositionInShip: 0,
      },
    });

    const direction = selected[0].split("");
    const directionToNum = letters[direction[0]].value;
    if (letterToNum > directionToNum) {
      for (let i = 1; i <= ship.size; i++) {
        const position = letterToNum - i;
        board.fields[position - 1][parseInt(splitted[1]) - 1] = {
          ship,
          hasBeenHit: false,
        };
        telemetryWorker.postMessage({
          eventName: "Player_PlaceShipPosition",
          properties: {
            Position: `${position}`,
            Ship: ship.name,
            PositionInShip: i,
          },
        });
      }
      console.log(board.fields);

      return;
    }
    if (letterToNum < directionToNum) {
      for (let i = 1; i <= ship.size; i++) {
        const position = letterToNum + i;
        board.fields[position - 1][parseInt(splitted[1]) - 1] = {
          ship,
          hasBeenHit: false,
        };
        telemetryWorker.postMessage({
          eventName: "Player_PlaceShipPosition",
          properties: {
            Position: `${position}`,
            Ship: ship.name,
            PositionInShip: i,
          },
        });
      }
      console.log(board.fields);

      return;
    }

    if (parseInt(direction[1]) < parseInt(splitted[1])) {
      for (let i = 1; i <= ship.size; i++) {
        const position = parseInt(splitted[1]) - i;
        this.board.fields[letterToNum - 1][position - 1] = {
          ship,
          hasBeenHit: false,
        };
        telemetryWorker.postMessage({
          eventName: "Player_PlaceShipPosition",
          properties: {
            Position: `${position}`,
            Ship: ship.name,
            PositionInShip: i,
          },
        });
      }
      console.log(board.fields);

      return;
    }
    if (parseInt(direction[1]) > parseInt(splitted[1])) {
      for (let i = 1; i <= ship.size; i++) {
        const position = parseInt(splitted[1]) + i;
        board.fields[letterToNum - 1][position - 1] = {
          ship,
          hasBeenHit: false,
        };
        telemetryWorker.postMessage({
          eventName: "Player_PlaceShipPosition",
          properties: {
            Position: `${position}`,
            Ship: ship.name,
            PositionInShip: i,
          },
        });
      }
      console.log(board.fields);
      return;
    }
  } else {
    console.log("The ship cannot be fit here");
    checkInput(ship, board, telemetryWorker);
  }
};

module.exports = checkInput;
